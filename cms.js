require('dotenv').config();
var express = require('express');
var cors = require('cors');
var app = express();
var port = process.env.PORT
const helmet = require('helmet');
const bodyParser = require("body-parser");
const logger = require('morgan');

global.__basedir = __dirname;

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Cors
var corsOption = {
  origin: "*",
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
};

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// routes
var promotionRoutes = require('./app/routes/promotion.routes.js');
var triviaRoutes = require('./app/routes/trivia.routes.js');
var prizesRoutes = require('./app/routes/prizes.routes.js');
var infoRoutes = require('./app/routes/info.routes.js');
var toupupRoutes = require('./app/routes/topup.routes.js');
var tutorialRoutes = require('./app/routes/tutorial.routes.js');
var memberRoutes = require('./app/routes/member.routes');
var dailyRoutes = require('./app/routes/daily.routes');

app.use(promotionRoutes);
app.use(prizesRoutes);
app.use(infoRoutes);
app.use('/trivia', triviaRoutes);
app.use('/topup', toupupRoutes);
app.use('/tutorial', tutorialRoutes);
app.use('/member', memberRoutes);
app.use('/daily', dailyRoutes);
app.use((req, res) => res.status(404).json(
  {
    "status": 404,
    "message": "Request not found",
  }
));

app.use(cors(corsOption));
app.use(helmet())

// Listening
app.get('/', function (req, res) {
  console.log('route / is accessed.');
  res.send('CMS API X-TRIVIA');
});

app.listen(port);

module.exports = app;