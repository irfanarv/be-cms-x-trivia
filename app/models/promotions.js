'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class promotions extends Model {
        static associate(models) {
        }
    };
    promotions.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name: DataTypes.STRING,
        image: DataTypes.STRING,
        image_home: DataTypes.STRING,
        date_start: DataTypes.DATE,
        date_end: DataTypes.DATE,
        action: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'promotions',
    });
    return promotions;
};
