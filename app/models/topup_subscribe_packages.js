'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class topup_subscribe_packages extends Model {
        static associate(models) {
        }
    };
    topup_subscribe_packages.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        telco: DataTypes.STRING,
        sdc: DataTypes.STRING,
        keyword: DataTypes.STRING,
        price: DataTypes.STRING,
        lucky_spin_bonus: DataTypes.INTEGER,
        image_langganan: DataTypes.STRING,
        image_berhenti: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'topup_subscribe_packages',
    });
    return topup_subscribe_packages;
};
