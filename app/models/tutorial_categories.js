'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class tutorial_categories extends Model {
        static associate(models) {
        }
    };
    tutorial_categories.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        title: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'tutorial_categories',
    });
    return tutorial_categories;
};
