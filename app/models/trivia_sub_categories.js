'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class trivia_sub_categories extends Model {
        static associate(models) {
            trivia_sub_categories.belongsTo(models.trivia_categories, {
                foreignKey: 'trivia_category_id',
                as: 'trivia_category'
            });
        }
    };
    trivia_sub_categories.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name: DataTypes.STRING,
        image: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'trivia_sub_categories',
    });
    return trivia_sub_categories;
};
