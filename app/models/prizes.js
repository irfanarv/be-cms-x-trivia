'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class prizes extends Model {
        static associate(models) {
        }
    };
    prizes.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name: DataTypes.STRING,
        image: DataTypes.STRING,
        redeem_point: DataTypes.INTEGER,
        winner_total: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'prizes',
    });
    return prizes;
};
