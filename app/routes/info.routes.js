const express = require('express');
const router = express.Router();
const info = require("../api").info;

router.post(
    "/info",
    info.add
);

module.exports = router