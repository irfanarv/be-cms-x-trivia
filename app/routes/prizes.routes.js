const express = require('express');
const router = express.Router();
const prizes = require("../api").prizes;

router.post(
    "/prizes",
    prizes.add
);

router.get(
    "/prizes/:name",
    prizes.getFile
);

module.exports = router