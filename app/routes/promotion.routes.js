const express = require('express');
const router = express.Router();
const promotion = require("../api").promotion;
const validate = require('../middleware/validate.middleware')

router.post(
    "/promotion",
    promotion.add
);

router.get(
    "/promotion/:name",
    promotion.getFile
);

module.exports = router