const express = require('express');
const router = express.Router();
const daily = require("../api").imageDaily;

router.get("/:name", daily.getFile);

module.exports = router