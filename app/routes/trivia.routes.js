const express = require('express');
const router = express.Router();
const triviaCategory = require("../api").triviaCategory;
const triviaSubCategory = require("../api").triviaSubCategory;
const triviaQuestion = require("../api").triviaQuestion
const triviaQuestionDetails = require("../api").triviaQuestionDetails

const validateAnswer = require("../middleware/validate.middleware").answerQuestion

router.post(
    "/category",
    triviaCategory.add
);

router.get(
    "/category/:name",
    triviaCategory.getFile
);

router.post(
    "/sub-category",
    triviaSubCategory.add
);

router.get(
    "/sub-category/:name",
    triviaSubCategory.getFile
);

router.post(
    "/question",
    triviaQuestion.add
);

router.post(
    "/question/details",
    validateAnswer,
    triviaQuestionDetails.add
);

router.get(
    "/question-assets/:name",
    triviaQuestion.getAssetQuestion
);


module.exports = router