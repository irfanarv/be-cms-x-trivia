const express = require("express");
const router = express.Router();
const tutorial = require("../api").tutorial;
const categoryTutorial = require("../api").categoryTutorial;

router.post("/", tutorial.add);

router.get("/:name", tutorial.getFile);

router.post("/categories", categoryTutorial.add);

module.exports = router;
