const express = require("express");
const router = express.Router();
const profile = require("../api").profile;

router.get("/profile/:name", profile.getFile);

module.exports = router;
