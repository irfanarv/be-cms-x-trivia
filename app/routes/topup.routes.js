const express = require('express');
const router = express.Router();
const dcbPackages = require("../api").dcbPackages;
const subscribePackages = require("../api").subscribePackages

router.post(
    "/dcb-packages",
    dcbPackages.add
);

router.get(
    "/dcb-packages/:name",
    dcbPackages.getFile
);

router.post(
    "/subscribe-packages",
    subscribePackages.add
);

router.get(
    "/subscribe-packages/:name",
    subscribePackages.getFile
);

module.exports = router