module.exports.promoName = function (req, res, next) {

    try {
        if (!req.body.name) {
            const response = {
                "status": 400,
                "success": false,
                "message": "Judul promo wajib diisi !"
            }
            return res.status(400).send(response)
        }
        next();
    }
    catch (err) {
        const response = {
            "status": 400,
            "success": false,
            "message": "Bad Request",
            "data": {
                "errors": err.message
            }
        }
        return res.status(400).send(response)
    }

};

module.exports.answerQuestion = function (req, res, next) {

    try {
        if (!req.query.trivia_question_id) {
            const response = {
                "status": 400,
                "success": false,
                "message": "Pilih pertanyaan dulu!"
            }
            return res.status(400).send(response)
        } else if (!req.body.answer_text) {
            const response = {
                "status": 400,
                "success": false,
                "message": "Masukan Jawaban!"
            }
            return res.status(400).send(response)
        } else if (!req.body.is_correct) {
            const response = {
                "status": 400,
                "success": false,
                "message": "Pilih status jawaban!"
            }
            return res.status(400).send(response)
        }
        next();
    }
    catch (err) {
        const response = {
            "status": 400,
            "success": false,
            "message": "Bad Request",
            "data": {
                "errors": err.message
            }
        }
        return res.status(400).send(response)
    }

};

