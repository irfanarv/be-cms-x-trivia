const Info = require("../../models").infos;

module.exports = {
    async add(req, res) {
    try {
        if (!req.body.type_info) {
            const response = {
              "status": 400,
              "success": false,
              "message": "Tipe info wajib diisi!"
            }
            return res.status(400).send(response)
          }
          if (!req.body.title_info) {
            const response = {
              "status": 400,
              "success": false,
              "message": "Judul info wajib diisi!"
            }
            return res.status(400).send(response)
          }
          if (!req.body.text_info) {
            const response = {
              "status": 400,
              "success": false,
              "message": "Konten info wajib diisi!"
            }
            return res.status(400).send(response)
          }
          await Info.create({
              type: req.body.type_info,
              title: req.body.title_info,
              text: req.body.text_info
          }).then(results => {
              const response = {
                  status: 200,
                  success: true,
                  message: "Data berhasil ditambahkan!"
              }
              return res.status(200).send(response)
          })
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
