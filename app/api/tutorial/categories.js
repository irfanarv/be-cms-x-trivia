const Categories = require("../../models").tutorial_categories;

module.exports = {
  async add(req, res) {
    try {
      if (!req.body.title) {
        const response = {
          status: 400,
          success: false,
          message: "Nama Kategori wajib diisi !",
        };
        return res.status(400).send(response);
      } else {
        await Categories.create({
          title: req.body.title,
        });
        const response = {
          status: 200,
          success: true,
          message: "Kategori tutorial berhasil ditambahkan!",
          data: {
            title: req.body.title
          },
        };
        return res.status(200).send(response);
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
