const dbcPackage = require("../../models").topup_dcb_packages;
const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../../../uploads/topup"));
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single("image");

let uploadFileMiddleware = util.promisify(uploadFile);

module.exports = {
  async add(req, res) {
    try {
      await uploadFileMiddleware(req, res);
      if (req.file == undefined) {
        const response = {
          status: 400,
          success: false,
          message: "Thumbail Topup wajib diisi!",
        };
        return res.status(400).send(response);
      } else {
        if (!req.body.amount) {
          const response = {
            status: 400,
            success: false,
            message: "Jumlah Credit Point tidak boleh kosong!",
          };
          return res.status(400).send(response);
        }
        if (!req.body.price) {
          const response = {
            status: 400,
            success: false,
            message: "Harga Credit Point tidak boleh kosong!",
          };
          return res.status(400).send(response);
        }
        if (!req.body.lucky_spin_bonus) {
          const response = {
            status: 400,
            success: false,
            message: "Bonus Lucky Spin tidak boleh kosong!",
          };
          return res.status(400).send(response);
        }
        await dbcPackage.create({
          amount: req.body.amount,
          price: req.body.price,
          lucky_spin_bonus: req.body.lucky_spin_bonus,
          image: req.file.filename,
        }).then((results) => {
          const response = {
            status: 200,
            success: true,
            message: "Top up DBC Packages Berhasil ditambahkan!",
            data: {
              jumlahCredit: req.body.amount,
              hargaTopup: req.body.price,
              spinBonus: req.body.lucky_spin_bonus,
              imgName: req.file.filename
            },
          };
          return res.status(200).send(response);
        });
      }
    } catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran thumbnail tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },

  async getFile(req, res) {
    try {
      const fileName = req.params.name;
      const directoryPath = __basedir + "/uploads/topup/";
      res.sendFile(directoryPath + fileName, fileName);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
