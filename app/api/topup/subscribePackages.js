const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const Subcribe = require("../../models").topup_subscribe_packages;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../../../uploads/topup"));
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFileCategory = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).fields([{ name: "image_langganan" }, { name: "image_berhenti" }]);

let uploadFileMiddleware = util.promisify(uploadFileCategory);

module.exports = {
  async add(req, res) {
    try {
      await uploadFileMiddleware(req, res);
      if (req.files["image_langganan"] == undefined) {
        const response = {
          status: 400,
          success: false,
          message: "Thumbnail dengan tombol berlangganan wajib diisi!",
        };
        return res.status(400).send(response);
      } else if (req.files["image_berhenti"] == undefined) {
        const response = {
          status: 400,
          success: false,
          message: "Thumbnail dengan tombol berhenti berlangganan wajib diisi!",
        };
        return res.status(400).send(response);
      } else {
        if (!req.body.telco) {
          const response = {
            status: 400,
            success: false,
            message: "Provider Telco wajib diisi!",
          };
          return res.status(400).send(response);
        }
        if (!req.body.sdc) {
          const response = {
            status: 400,
            success: false,
            message: "SDC wajib diisi!",
          };
          return res.status(400).send(response);
        }
        if (!req.body.keyword) {
          const response = {
            status: 400,
            success: false,
            message: "Keyword Berlangganan wajib diisi!",
          };
          return res.status(400).send(response);
        }
        if (!req.body.price) {
          const response = {
            status: 400,
            success: false,
            message: "Harga Credit Point tidak boleh kosong!",
          };
          return res.status(400).send(response);
        }
        if (!req.body.lucky_spin_bonus) {
          const response = {
            status: 400,
            success: false,
            message: "Bonus Lucky Spin tidak boleh kosong!",
          };
          return res.status(400).send(response);
        }
        await Subcribe.create({
          telco: req.body.telco,
          sdc: req.body.sdc,
          keyword: req.body.keyword,
          price: req.body.price,
          lucky_spin_bonus: req.body.lucky_spin_bonus,
          image_langganan: req.files.image_langganan[0].filename,
          image_berhenti: req.files.image_berhenti[0].filename,
        });
        const response = {
          status: 200,
          success: true,
          data: {
            telco: req.body.telco,
            sdc: req.body.sdc,
            keyword: req.body.keyword,
            price: req.body.price,
            spinBonus: req.body.lucky_spin_bonus,
            imageSubscribe: req.files.image_langganan[0].filename,
            imageUnsubscribe: req.files.image_berhenti[0].filename,
          },
        };
        return res.status(200).send(response);
      }
    } catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },

  async getFile(req, res) {
    try {
      const fileName = req.params.name;
      const directoryPath = __basedir + "/uploads/topup/";
      res.sendFile(directoryPath + fileName, fileName);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
