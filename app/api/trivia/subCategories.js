const subCategory = require('../../models').trivia_sub_categories
const Categories = require('../../models').trivia_categories
const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(
      null,
      path.join(__dirname, "../../../uploads/trivia/sub_category")
    );
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single("image");

let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = {
  async add(req, res) {
    try {
      const checkCategory = await Categories.findOne({
        where: {
          id: req.query.category_id
        }
      });
      if (!checkCategory) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Kategori trivia tidak ditemukan!",
        }
        return res.status(400).send(response)
      } else {
        await uploadFileMiddleware(req, res)
        if (req.file == undefined) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Thumbnail Sub Kategori wajib diupload !",
          }
          return res.status(400).send(response)
        } else {
          if (!req.body.name) {
            const response = {
              "status": 400,
              "success": false,
              "message": "Judul Sub Kategori tidak boleh kosong !",
            }
            return res.status(400).send(response)
          }
          if (!req.query.category_id) {
            const response = {
              "status": 400,
              "success": false,
              "message": "Kategori tidak boleh kosong !",
            }
            return res.status(400).send(response)
          }
          await subCategory.create({
            name: req.body.name,
            trivia_category_id: req.query.category_id,
            image: req.file.filename,
          });
          const response = {
            "status": 200,
            "success": true,
            "data": {
              "name": req.body.name,
              "thumbnail": req.file.filename,
            }
          }
          return res.status(200).send(response)
        }
      }
    }
    catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },

  async getFile(req, res) {
    try {
      const fileName = req.params.name
      const directoryPath = __basedir + "/uploads/trivia/sub_category/";
      res.sendFile(directoryPath + fileName, fileName);
    }
    catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  }

}
