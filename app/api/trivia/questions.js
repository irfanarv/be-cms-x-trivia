const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const Question = require('../../models').trivia_questions
const subCategory = require('../../models').trivia_sub_categories

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(
      null,
      path.join(__dirname, "../../../uploads/trivia/questions")
    );
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadImage = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single('image');

let uploadImageMiddleware = util.promisify(uploadImage);

module.exports = {

  async add(req, res) {
    try {
      const checkSubCategories = await subCategory.findOne({
        where: {
          id: req.query.sub_category_id
        }
      });
      if (!checkSubCategories) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Sub Kategori trivia tidak ditemukan!",
        }
        return res.status(400).send(response)
      } else {
        await uploadImageMiddleware(req, res)
        if (!req.body.title) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Judul Pertanyaan wajib diisi !",
          }
          return res.status(400).send(response)
        } else if (!req.query.sub_category_id) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Sub Kategori tidak boleh kosong !",
          }
          return res.status(400).send(response)
        }
        if (!req.file) {
          await Question.create({
            trivia_sub_category_id: req.query.sub_category_id,
            title: req.body.title
          });
          const response = {
            "status": 200,
            "success": true,
            "data": {
              "subCategories": checkSubCategories.name,
              "title": req.body.title
            }
          }
          return res.status(200).send(response)
        } else {
          await Question.create({
            trivia_sub_category_id: req.query.sub_category_id,
            title: req.body.title,
            image: req.file.filename,
          });
          const response = {
            "status": 200,
            "success": true,
            "data": {
              "subCategories": checkSubCategories.name,
              "title": req.body.title,
              "image": req.file.filename
            }
          }
          return res.status(200).send(response)
        }
      }

    }
    catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },

  async getAssetQuestion(req, res){
    try {
      const fileName = req.params.name
      const directoryPath = __basedir + "/uploads/trivia/questions/";
      res.sendFile(directoryPath + fileName, fileName);
    }
    catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  }

}

