const Question = require('../../models').trivia_questions
const questionDetails = require('../../models').trivia_question_details

module.exports = {
    async add(req, res) {
        try {
            const checkQuestion = await Question.findOne({
                where: {
                    id: req.query.trivia_question_id
                }
            });
            if (!checkQuestion) {
                const response = {
                    "status": 400,
                    "success": false,
                    "message": "Pertanyaan tidak ditemukan!",
                }
                return res.status(400).send(response)
            } else {
                await questionDetails.create({
                    trivia_question_id: req.query.trivia_question_id,
                    answer_text: req.body.answer_text,
                    sort_no: req.body.sort_no,
                    is_correct: req.body.is_correct
                });
                const response = {
                    "status": 200,
                    "success": true,
                    "message": "Jawaban berhasil ditambahkan!",
                    "data": {
                        "question": checkQuestion.title,
                        "answer": req.body.answer_text,
                        "sortNo": req.body.sort_no,
                        "isCorrect": req.body.is_correct,
                    }
                }
                return res.status(200).send(response)
            }
        }
        catch (err) {
            const response = {
                "status": 400,
                "success": false,
                "message": "Bad Request",
                "data": {
                    "errors": err.message
                }
            }
            return res.status(400).send(response)
        }
    }
}