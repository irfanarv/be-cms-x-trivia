const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const Categories = require('../../models').trivia_categories

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(
      null,
      path.join(__dirname, "../../../uploads/trivia/category")
    );
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFileCategory = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).fields([{ name: "trivia_cat" }, { name: "trivia_cat_active" }]);

let uploadFileMiddleware = util.promisify(uploadFileCategory);

module.exports = {

  async add(req, res) {
    try {
      await uploadFileMiddleware(req, res)
      if (req.files['trivia_cat'] == undefined) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Thumbnail kategori wajib diisi!",
        }
        return res.status(400).send(response)
      }
      else if (req.files['trivia_cat_active'] == undefined) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Thumbnail kategori aktif wajib diisi!",
        }
        return res.status(400).send(response)
      } else {
        if (!req.body.name) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Judul Kategori tidak boleh kosong !",
          }
          return res.status(400).send(response)
        }
        await Categories.create({
          name: req.body.name,
          image: req.files.trivia_cat[0].filename,
          image_active: req.files.trivia_cat_active[0].filename,
        });
        const response = {
          "status": 200,
          "success": true,
          "data": {
            "name": req.body.name,
            "image": req.files.trivia_cat[0].filename,
            "imageActive": req.files.trivia_cat_active[0].filename,
          }
        }
        return res.status(200).send(response)
      }
    }
    catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },

  async getFile(req, res){
    try {
      const fileName = req.params.name
      const directoryPath = __basedir + "/uploads/trivia/category/";
      res.sendFile(directoryPath + fileName, fileName);
    }
    catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  }

}

