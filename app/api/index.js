const promotion = require('./promotions');
const triviaCategory = require('./trivia/categories')
const triviaSubCategory = require('./trivia/subCategories')
const triviaQuestion = require('./trivia/questions')
const triviaQuestionDetails = require('./trivia/questionDetails')
const prizes = require('./prizes')
const info = require('./info')
const dcbPackages = require('./topup/dcbPackages')
const subscribePackages = require('./topup/subscribePackages')
const tutorial = require('./tutorial/tutorial')
const categoryTutorial = require('./tutorial/categories')
const profile = require('./member/profile')
const imageDaily = require('./daily')

module.exports = {
    promotion,
    triviaCategory,
    triviaSubCategory,
    triviaQuestion,
    triviaQuestionDetails,
    prizes,
    info,
    dcbPackages,
    subscribePackages,
    tutorial,
    categoryTutorial,
    profile,
    imageDaily,
};