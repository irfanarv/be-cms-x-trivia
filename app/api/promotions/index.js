const Promotions = require('../../models').promotions
const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(
      null,
      path.join(__dirname, "../../../uploads/promo")
    );
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).fields([{ name: "banner_promo" }, { name: "banner_promo_home" }]);

let uploadFileMiddleware = util.promisify(uploadFile);

module.exports = {
  async add(req, res) {
    try {
      await uploadFileMiddleware(req, res)
      if (req.files['banner_promo'] == undefined) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Banner Promo wajib diupload !",
        }
        return res.status(400).send(response)
      } else if (req.files['banner_promo_home'] == undefined) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Banner Promo home wajib diupload !",
        }
        return res.status(400).send(response)
      } else {
        if (!req.body.name) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Judul promo tidak boleh kosong !",
          }
          return res.status(400).send(response)
        }
        if (!req.body.date_start || !req.body.date_end) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Masa berlaku promo tidak boleh kosong !",
          }
          return res.status(400).send(response)
        }
        if (!req.body.action) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Tindakan banner promo tidak boleh kosong!",
          }
          return res.status(400).send(response)
        }
        await Promotions.create({
          name: req.body.name,
          image: req.files.banner_promo[0].filename,
          image_home: req.files.banner_promo_home[0].filename,
          date_start: req.body.date_start,
          date_end: req.body.date_end,
          action: req.body.action
        });
        const response = {
          "status": 200,
          "success": true,
          "data": {
            "promoName": req.body.name,
            "bannerName": req.files.banner_promo[0].filename,
            "bannerHome": req.files.banner_promo_home[0].filename,
            "dateStart": req.body.date_start,
            "dateEnd": req.body.date_end,
            "action": req.body.action
          }
        }
        return res.status(200).send(response)
      }


    }
    catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },

  async getFile(req, res){
    try {
      const fileName = req.params.name
      const directoryPath = __basedir + "/uploads/promo/";
      res.sendFile(directoryPath + fileName, fileName);
    }
    catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  }

}
