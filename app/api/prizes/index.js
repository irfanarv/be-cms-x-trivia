const Prizes = require('../../models').prizes
const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(
      null,
      path.join(__dirname, "../../../uploads/prizes")
    );
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single("image");

let uploadFileMiddleware = util.promisify(uploadFile);

module.exports = {
  async add(req, res) {
    try {
      await uploadFileMiddleware(req, res)
      if (req.file == undefined) {
        const response = {
          "status": 400,
          "success": false,
          "message": "Thumbail hadiah wajib diisi!",
        }
        return res.status(400).send(response)
      } else {
        if (!req.body.name) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Nama hadiah tidak boleh kosong!"
          }
          return res.status(400).send(response)
        }
        if (!req.body.redeem_point) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Jumlah Redeem Poin tidak boleh kosong!"
          }
          return res.status(400).send(response)
        }
        if (!req.body.winner_total) {
          const response = {
            "status": 400,
            "success": false,
            "message": "Jumlah pemenang tidak boleh kosong!"
          }
          return res.status(400).send(response)
        }
        await Prizes.create({
          name: req.body.name,
          image: req.file.filename,
          redeem_point: req.body.redeem_point,
          winner_total: req.body.winner_total
        })
          .then(results => {
            const response = {
              "status": 200,
              "success": true,
              "data": {
                "prizeName": req.body.name,
                "imgName": req.file.filename,
                "redeemPoint": req.body.redeem_point,
                "winnerTotal": req.body.winner_total
              }
            }
            return res.status(200).send(response)
          })
      }
    }
    catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },

  async getFile(req, res) {
    try {
      const fileName = req.params.name
      const directoryPath = __basedir + "/uploads/prizes/";
      res.sendFile(directoryPath + fileName, fileName);
    }
    catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  }

}
