module.exports = {
    async getFile(req, res) {
      try {
        const fileName = req.params.name;
        const directoryPath = __basedir + "/uploads/daily/";
        res.sendFile(directoryPath + fileName, fileName);
      } catch (err) {
        const response = {
          status: 400,
          success: false,
          message: "Bad Request",
          data: {
            errors: err.message,
          },
        };
        return res.status(400).send(response);
      }
    },
  };
  